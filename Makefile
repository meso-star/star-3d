# Copyright (C) 2015-2023 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libs3d.a
LIBNAME_SHARED = libs3d.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Star-3D building
################################################################################
SRC =\
 src/s3d_device.c\
 src/s3d_geometry.c\
 src/s3d_instance.c\
 src/s3d_mesh.c\
 src/s3d_primitive.c\
 src/s3d_scene.c\
 src/s3d_scene_view.c\
 src/s3d_scene_view_closest_point.c\
 src/s3d_scene_view_trace_ray.c\
 src/s3d_shape.c\
 src/s3d_sphere.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f "$${i}"; done)\
 $$(if [ -n "$(LIBNAME)" ]; then\
      echo "$(LIBNAME)";\
    else\
      echo "$(LIBNAME_SHARED)";\
    fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) -std=c99 $(CFLAGS_SO) $(DPDC_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(DPDC_LIBS)

$(LIBNAME_STATIC): libs3d.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libs3d.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: Makefile config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then\
		echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(EMBREE_VERSION) embree4; then\
		echo "embree $(EMBREE_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) -std=c99 $(CFLAGS_SO) $(DPDC_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) -std=c99 $(CFLAGS_SO) $(DPDC_CFLAGS) -DS3D_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@EMBREE_VERSION@#$(EMBREE_VERSION)#g'\
	    s3d.pc.in > s3d.pc

s3d-local.pc: s3d.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@EMBREE_VERSION@#$(EMBREE_VERSION)#g'\
	    s3d.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" s3d.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/star" src/s3d.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/star-3d"\
		COPYING README.md

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/s3d.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-3d/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-3d/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/s3d.h"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(LIBNAME)
	rm -f .config .test libs3d.o s3d.pc s3d-local.pc

distclean: clean
	rm -f $(DEP) $(TEST_DEP)

lint:
	shellcheck -o all make.sh

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_s3d_accel_struct_conf.c\
 src/test_s3d_closest_point.c\
 src/test_s3d_device.c\
 src/test_s3d_primitive.c\
 src/test_s3d_sampler.c\
 src/test_s3d_sample_sphere.c\
 src/test_s3d_scene.c\
 src/test_s3d_scene_view_aabb.c\
 src/test_s3d_scene_view.c\
 src/test_s3d_seams.c\
 src/test_s3d_shape.c\
 src/test_s3d_sphere_box.c\
 src/test_s3d_sphere.c\
 src/test_s3d_sphere_instance.c\
 src/test_s3d_trace_ray.c\
 src/test_s3d_trace_ray_instance.c\
 src/test_s3d_trace_ray_sphere.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
S3D_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags s3d-local.pc)
S3D_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs s3d-local.pc)

build_tests: build_library $(TEST_DEP) .test
	@$(MAKE) -fMakefile -f.test \
	$$(for i in $(TEST_DEP); do echo -f"$${i}"; done) test_bin

test: build_tests
	@$(SHELL) make.sh run_test $(TEST_SRC)

.test: Makefile
	@$(SHELL) make.sh config_test $(TEST_SRC) > .test

clean_test:
	@$(SHELL) make.sh clean_test $(TEST_SRC)

$(TEST_DEP): config.mk s3d-local.pc
	@$(CC) -std=c89 $(CFLAGS_EXE) $(S3D_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_OBJ): config.mk s3d-local.pc
	$(CC) -std=c89 $(CFLAGS_EXE) $(S3D_CFLAGS) -c $(@:.o=.c) -o $@

test_s3d_device \
test_s3d_primitive \
test_s3d_sampler \
test_s3d_scene \
test_s3d_scene_view_aabb \
test_s3d_scene_view \
test_s3d_shape \
: config.mk s3d-local.pc $(LIBNAME)
	$(CC) -std=c89 $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(S3D_LIBS)

test_s3d_accel_struct_conf \
test_s3d_closest_point \
test_s3d_sample_sphere \
test_s3d_seams \
test_s3d_sphere_box \
test_s3d_sphere \
test_s3d_sphere_instance \
test_s3d_trace_ray \
test_s3d_trace_ray_instance \
test_s3d_trace_ray_sphere \
: config.mk s3d-local.pc $(LIBNAME)
	$(CC) -std=c89 $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(S3D_LIBS) -lm
